<?php

$to = "all@masteryconnect.com";
$subject = "Interest In More Info!";
$body = "Somebody showed interest in finding out more about MasteryConnect.\n\n";
$body .= "\r\nNAME     : ".$_POST['data']['name'];
$body .= "\r\nTITLE    : ".$_POST['data']['title'];
$body .= "\r\nPHONE    : ".$_POST['data']['phone'][0].'-'.$_POST['data']['phone'][1].'-'.$_POST['data']['phone'][2];
$body .= "\r\nEMAIL    : ".$_POST['data']['email'];
$body .= "\r\nSCHOOL : ".$_POST['data']['school'];
$body .= "\r\nDISTRICT : ".$_POST['data']['district'];
$body .= "\r\nSTATE    : ".$_POST['data']['state'];
$body .= "\r\n";
$body .= "\r\nFOUND BY : ".$_POST['data']['found_by'];
$body .= "\r\n";
$body .= "\r\nEVENT SOURCE: ".$_COOKIE['event_source'];
$headers = 'From: noreply@masteryconnect.com'."\r\n".
  'Reply-To: noreply@masteryconnect.com'."\r\n".
  'X-Mailer: PHP/' . phpversion();

if (mail($to, $subject, $body, $headers)) {
  header("Location: ".$_POST['redirect']);
}

?>
