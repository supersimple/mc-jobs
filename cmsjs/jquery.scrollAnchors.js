/*
 *  Scroll Anchors
 *
 *  This will make the in page anchors bounce into place.
 *  You can pass any of the options available to the scrollTo plugin.
 *
 *  Dependencies:
 *    jQuery
 *    jQuery.scrollTo
 *    jQuery.easing
 *
 */
(function($) {
  $.scrollAnchors = function(options) {
    $('a[href*=#]').live('click',function() {
      var name = $(this).attr('href').split('#')[1];
      var pos = $(window).scrollTop();
      window.location.hash = name;
      $(window).scrollTop(pos);
      $.scrollTo('a[name='+name+']', $.extend({duration: 1000, easing: 'easeOutSine'},options));
      return false;
    });
  };
})(jQuery);
