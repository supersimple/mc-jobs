var _MC_embed_ccapp = function() {
  var container = document.getElementById('_MC_ccapp_embed');
  var showWidget = function() {
    container.innerHTML = '<iframe src="http://ccapp.masteryconnect.com/" frameborder="0" width="480" height="600"></iframe>';
  };
  var showItunesApp = function() {
    container.innerHTML = '<iframe src="http://ccapp.masteryconnect.com/ios_link.html" frameborder="0" width="480" height="600"></iframe>';
  };
  var showAndroidApp = function() {
    container.innerHTML = '<iframe src="http://ccapp.masteryconnect.com/android_link.html" frameborder="0" width="480" height="600"></iframe>';
  };

  var platforms = {
    'iOS': { reg: /(iPad|iPhone)/i, action: showItunesApp },
    'OSX': { reg: /Mac/i, action: showWidget },
    'android': { reg: /Linux arm/i, action: showAndroidApp },
    'windows': { reg: /win/i, action: showWidget },
    'linux': { reg: /(i386|i686|x86_64)/i, action: showWidget }
  };
  for (var i in platforms) {
    if (platforms[i]['reg'] && platforms[i].reg.test(navigator.platform)) {
      platforms[i].action();
      break;
    }
  }
};

if (window.onload) {
  var tmp = window.onload;
  window.onload = function() {
    tmp();
    _MC_embed_ccapp();
  }
} else {
  window.onload = function() {
    _MC_embed_ccapp();
  }
}
