(function($){

  var methods = {
    init: function( options ) {
      return this.each(function() {
        var $this = $(this);

        // set the width of the slider_panels element
        var width = $this.width();
        var num_of_panels = $('.slider_bullet',this).length;
        $('.slider_panels',this).width(width*num_of_panels);
        $('.slider_panel',this).show();
        $('.slider_viewport',this).attr('scrollLeft',0);
        $this.data('width',width);
        $this.data('num_of_panels',num_of_panels);

        // hook up to the bullet clicks
        $('.slider_bullet').click(function() {
          var index = $(this).prevAll().length;
          $this.bulletSlider('slideTo',index);
          $this.bulletSlider('stopAutoPlay');
          $(this).blur();
          return false;
        });

        // select the first bullet
        $('.slider_bullet:first').addClass('selected');
      });
    },
    slideTo: function(index) {
      return this.each(function() {
        var $this = $(this);

        var width = $this.data('width');
        var num_of_panels = $this.data('num_of_panels');

        // slide the viewport
        $('.slider_viewport',this).animate({scrollLeft: (width*index)},500);

        // select the bullet
        $('.slider_bullet.selected').removeClass('selected');
        $('.slider_bullet:eq('+index+')').addClass('selected');

      });
    },
    startAutoPlay: function() {
      return this.each(function() {
        var $this = $(this);
        var index = $('.slider_bullet.selected',this).prevAll().length;

        // start the timer
        $this.data('timer_index',index);
        $this.data('timer_count',0);
        clearTimeout($this.data('timer'));
        $this.data('timer',setInterval(function() {
          var next_index = $this.data('timer_index')+1;
          
          if (next_index == $this.data('num_of_panels')) {
            next_index = 0;
            $this.data('timer_count',$this.data('timer_count')+1);
            if ($this.data('timer_count') == 2) {
              clearTimeout($this.data('timer'));
            }
          }

          $this.bulletSlider('slideTo',next_index);
          $this.data('timer_index',next_index);
        },5000));
      });
    },
    stopAutoPlay: function() {
      return this.each(function() {
        var $this = $(this);

        // stop the timer
        clearTimeout($this.data('timer'));
      });
    }
  };

  $.fn.bulletSlider = function( method ) {
    if ( methods[method] ) {
      return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
    }    
  };

  $(function() {
    $('.bullet_slider').bulletSlider();
    $('.bullet_slider').bulletSlider('startAutoPlay');
  });
})(jQuery);
