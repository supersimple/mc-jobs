function swapVideo(video){
  window.location.hash = video;
  var $that = $("#video_"+video);
  init_video($that);
  swapSocialLinks(video)
}

function swapSocialLinks(video){
  $("#social_links div").hide();
  $("#sl_"+video).show();
  //update the facebook open graph meta tags
	switch(video)
	{
	case 'nutshell':
    $("#fb-title").attr('content','MasteryConnect in a nutshell.');
    $("#fb-image").attr('content','http://www.masteryconnect.com/learn-more/cmsimages/small-poster-nutshell.jpg');
    $("#fb-description").attr('content','Check out this awesome video!');
    $("#fb-url").attr('content','http://www.masteryconnect.com/#nutshell');
    
	break;
	case 'plc':
    $("#fb-title").attr('content','MasteryConnect - the PLC technology solution!');
    $("#fb-image").attr('content','http://www.masteryconnect.com/learn-more/cmsimages/small-poster-plc.jpg');
    $("#fb-description").attr('content','Check out this awesome video!');
    $("#fb-url").attr('content','http://www.masteryconnect.com/#plc');
	
	break;
	case 'intro':
    $("#fb-title").attr('content','An introduction to MasteryConnect.');
    $("#fb-image").attr('content','http://www.masteryconnect.com/learn-more/cmsimages/small-poster-intro.jpg');
    $("#fb-description").attr('content','Check out this awesome video!');
    $("#fb-url").attr('content','http://www.masteryconnect.com/#intro');
	  
	break;
	default:
    $("#fb-title").attr('content','MasteryConnect - Home');
    $("#fb-image").attr('content','http://www.masteryconnect.com/learn-more/cmsimages/mark.png');
    $("#fb-description").attr('content','MasteryConnect - Share common assessments aligned to standards, connect in a professional learning community, and track student performance.');
    $("#fb-url").attr('content','http://www.masteryconnect.com');
	}
}

function init_video($that){
    $('.dialog_content').empty().append('<p id="introvideo"><img src="/images/loading.gif" width="100" /></p>');
    var video = "http://files.masteryconnect.com/intro.m4v";
    if ($that.attr('href') != "#") {
      video = $that.attr('href');
    }
    // height 407|540 width 720
    var height = $that.attr('data-height');
    if (Number(height) != height) {
      height = 540;
    }
    //
    var width = $that.attr('data-width');
    if (Number(width) != width) {
      width = 720;
    }
	
    var video_title = $that.attr('data-title');
    if (!video_title) {
      video_title = 'MasteryConnect';
    }
	  
    if($that.attr('data-trigger')){ var data_trigger = $that.attr('data-trigger').substr(1,$that.attr('data-trigger').length); } else { var data_trigger = false; }
	  
    var jsbox = '\
<div class="video-js-box">\
  <video class="video-js" width="'+width+'" height="'+height+'" controls preload>\
    <source src="'+video+'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>\
    <object id="flash_fallback_1" class="vjs-flash-fallback" bgcolor="#000000" width="'+width+'" height="'+height+'" type="application/x-shockwave-flash" data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">\
      <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />\
      <param name="allowfullscreen" value="true" />\
	  <param name="bgcolor" value="#000000" />\
      <param name="flashvars" value=\'config={"playlist":[{"url": "'+video+'","autoPlay":true,"autoBuffering":true,"scaling":"fit"}]}\' />\
    </object>\
  </video>\
</div>\
';
if(data_trigger){
jsbox += '<div id="video_playlist">\
<a onclick="swapVideo(\'nutshell\')"><img src="cmsimages/small-poster-nutshell.jpg" alt="Nutshell" /></a>\
<a onclick="swapVideo(\'plc\')"><img src="cmsimages/small-poster-plc.jpg" alt="PLC" /></a>\
<a onclick="swapVideo(\'intro\')"><img src="cmsimages/small-poster-intro.jpg" alt="Intro" /></a>\
</div>\
<div id="social_links">\
<div id="sl_plc">\
<a target="_blank" href="http://pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.masteryconnect.com%2F%23plc&media=http%3A%2F%2Ffiles.masteryconnect.com%2Fvideo-plc.jpg&description=Check%20out%20this%20awesome%20video!%20The%20PLC%20technology%20solution%20-%20MasteryConnect."><img src="cmsimages/social_flag_pinterest.gif" alt="Pinterest" /></a>\
<a target="_blank" href="http://www.facebook.com/share.php?u=http%3A%2F%2Fwww.masteryconnect.com%2F%23plc"><img src="cmsimages/social_flag_facebook.gif" alt="Facebook" /></a>\
<a target="_blank" href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fwww.masteryconnect.com%2F%23plc&text=Check%20out%20this%20awesome%20video!%20The%20PLC%20Technology%20Solution%20-%20MasteryConnect."><img src="cmsimages/social_flag_twitter.gif" alt="Twitter" /></a>\
</div>\
<div id="sl_nutshell">\
<a target="_blank" href="http://pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.masteryconnect.com%2F%23nutshell&media=http%3A%2F%2Ffiles.masteryconnect.com%2Fvideo_nutshell.jpg&description=Check%20out%20this%20awesome%20video!%20MasteryConnect%20in%20a%20nutshell..."><img src="cmsimages/social_flag_pinterest.gif" alt="Pinterest" /></a>\
<a target="_blank" href="http://www.facebook.com/share.php?u=http%3A%2F%2Fwww.masteryconnect.com%2F%23nutshell"><img src="cmsimages/social_flag_facebook.gif" alt="Facebook" /></a>\
<a target="_blank" href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fwww.masteryconnect.com%2F%23nutshell&text=Check%20out%20this%20awesome%20video!%20MasteryConnect%20in%20a%20nutshell..."><img src="cmsimages/social_flag_twitter.gif" alt="Twitter" /></a>\
</div>\
<div id="sl_intro">\
<a target="_blank" href="http://pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.masteryconnect.com%2F%23intro&media=http%3A%2F%2Ffiles.masteryconnect.com%2Fvideo_start.jpg&description=Check%20out%20this%20awesome%20video!%20Introduction%20to%20MasteryConnect."><img src="cmsimages/social_flag_pinterest.gif" alt="Pinterest" /></a>\
<a target="_blank" href="http://www.facebook.com/share.php?u=http%3A%2F%2Fwww.masteryconnect.com%2F%23intro"><img src="cmsimages/social_flag_facebook.gif" alt="Facebook" /></a>\
<a target="_blank" href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fwww.masteryconnect.com%2F%23intro&text=Check%20out%20this%20awesome%20video!%20Introduction%20to%20MasteryConnect."><img src="cmsimages/social_flag_twitter.gif" alt="Twitter" /></a>\
</div>\
<em>Share this video</em>\
</div>\
<div style="width:100%;height:1px;clear:both;">&nbsp;</div>\
';
}else{
  jsbox += '<div style="width:100%;height:1px;clear:both;">&nbsp;</div>';
}

var video_js_box = $(jsbox);

    $('#introvideo').empty().append(video_js_box);
    $('.top_cap h1').text(video_title); //setup title
    $('#introvideo video').VideoJS(); //add analytics trigger in here
    if ($('video').get(0).player) {
      try {
        $('video').get(0).player.play();
      } catch(e) {}
      pageTracker._trackEvent("Videos", "Play", video_title);
    }
    swapSocialLinks(data_trigger);
	
}

$(function() {

	//init the hover state for the 3 boxes on the homepage
	$(".home .box.one-third").each(function(){
		$(this).hover(function(){ $(this).addClass('hover'); },function(){ $(this).removeClass('hover'); });
		
	});

  $('td.feature').not('div.info_window').hover(function(){
	$(this).children('div.info_window').fadeIn(200);  
    //$(this).children('div.info_window').delay(250).css('top','-93px').show(0,function(){$(this).animate({top:'+=6px',opacity:1.0},250);});
  },function(){
	$('.info_window').fadeOut(200);  
    //$(this).children('div.info_window').animate({top:'-=6px',opacity:0.0},250,function(){$(this).hide();});
  });
	
	//init the screenshots on the homepage
	$("#screenshot_dialog").jqm({trigger: ".open_screenshot_dialog", modal: true});
	$(".open_screenshot_dialog").click(function() {
    $(document).scrollTop(810);
    $('.screenshot_dialog_content').empty().append('<img src="'+$(this).attr('href')+'" />');
  });
  
  var flashVars = {
    movie: 'cmsflash/flashBanner.swf',
    xmlFile: 'cmsxml/banners.xml'
  };
  var params = {
    wmode: 'transparent',
    quality: 'high'
  };
  var attributes = {};
  swfobject.embedSWF("cmsflash/flashBanner.swf", "flash", "490", "340", "9.0.0",attributes,flashVars,params);

  $("#dialog").jqm({trigger: ".open_dialog", modal: true, onHide: function(hash) {
    $('#introvideo').empty();
    // jqm default action
    hash.w.hide();
    hash.o.remove();
  }});

  $(".open_dialog.form").click(function() {
    $(document).scrollTop(0);
    $('.dialog_content').empty().append('<img src="/images/loading.gif" width="100" />').load($(this).attr('href'));
    if ($(this).attr('href') == '/who_are_you') {
      $('.top_cap h1').text("Sign up");
    } else if ($(this).attr('href') == '/who_are_you?b=1') {
      $('.top_cap h1').text("Sign Up");
    } else {
      $('.top_cap h1').text("Sign Up");
    }
  });
  $(".open_dialog.video").click(function() {
    var $that = $(this);
    if ($('.bullet_slider').length > 0) {
      $('.bullet_slider').bulletSlider('stopAutoPlay');
    }
    $(document).scrollTop(0);
	
	init_video($that);
  });

  if (window.location.hash != '') {
    var sel = $('[data-trigger="'+window.location.hash+'"]');
    if (sel.length > 0) {
      setTimeout(function(){
        sel.click();
      },500);
    }
  }
  
  // signup form handlers
  $('.im_a_teacher').live('click',function() {
    $('.dialog_content')
      .empty()
      .append('<img src="/images/loading.gif" width="100" />')
      .load($(this).attr('href'));
    return false;
  });

  $('form#new_teacher input[name="subjects[]"][value="1"]').live('click',function() {
    if ($(this).attr('checked')) {
      $('#pathway_list').show();
    } else {
      $('#pathway_list').hide();
    }
  });

  $('#zip').live('keyup',function() {
    $('#zip').parent().removeClass('loading');

    if ($('#zip').val().length == 5) {
      $('#zip').parent().addClass('loading');
      $('#school_list').load('/schools_for_zip/'+$('#zip').val(),function() {
        $('#zip').parent().removeClass('loading');
      });
    }
  });
    
  function submitHandler() {
    var $that = $('form');
    $('.error').removeClass('error');

    // validate form data
    if ( $('#teacher_first_name').val() == "" ) {
      alert('Please enter a valid First Name.');
      $('#teacher_first_name').addClass('error');
      return false;
    }
    if ( $('#teacher_last_name').val() == "" ) {
      alert('Please enter a valid Last Name.');
      $('#teacher_last_name').addClass('error');
      return false;
    }
    if (!validEmail( $('#teacher_email').val() )) {
      alert('Please enter a valid School Email.');
      $('#teacher_email').addClass('error');
      return false;
    }
    if ($('#teacher_alt_email').val() != "" && !validEmail( $('#teacher_alt_email').val() )) {
      alert('Please enter a valid Alternative Email.');
      $('#teacher_alt_email').addClass('error');
      return false;
    }
    if ( $('#zip').val().length != 5 ) {
      alert('Please enter a valid Zip Code.');
      $('#zip').addClass('error');
      return false;
    }
    if ( !$('#school_list select').val() || $('#school_list select').val() == "" ) {
      alert('Please select a valid School.');
      $('#school_list select').addClass('error');
      return false;
    }
    
    // disable the button
    $(this).attr('disabled',true);

    var post_data = $that.serialize();
    $.post($that.attr('action'),post_data,function(results) {
      $('.dialog_content').empty().append(results);
    })
    return false;
  }

  $('form').live('submit',submitHandler);
  $('form button').live('click',submitHandler);
  if (window.VideoJS) {
    VideoJS.setupAllWhenReady();
  }
});

function validEmail(email) {
  return (/^\w+([\.\-+]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email));
}

function showFbModal(){
  resizeOverlay();
  $('#fb_box_overlay').css('background-color','#000000');
  $('#fb_box_overlay').fadeTo(75,0.50);
  $('#pd_content_outer').fadeIn(150);
  //scroll to top:200
  $('html,body').animate({
  scrollTop: 200
  }, 250);
}

function showCenteredFbModal(){
  resizeOverlay();
  $('#fb_box_overlay').css('background-color','#000000');
  $('#fb_box_overlay').fadeTo(75,0.50);
  
  //center the modal
  var viewport_height = $(window).height();
  var content_height = $("#pd_content_outer").height();
  var current_loc = getPageScroll();
  var center_pos = current_loc[1] + (viewport_height/2 - content_height/2)
  $("#pd_content_outer").css('top',center_pos);
  
  $('#pd_content_outer').fadeIn(150);
  
}

function getPageScroll() {
    var xScroll, yScroll;
    if (self.pageYOffset) {
      yScroll = self.pageYOffset;
      xScroll = self.pageXOffset;
    } else if (document.documentElement && document.documentElement.scrollTop) {
      yScroll = document.documentElement.scrollTop;
      xScroll = document.documentElement.scrollLeft;
    } else if (document.body) {// all other Explorers
      yScroll = document.body.scrollTop;
      xScroll = document.body.scrollLeft;
    }
    return new Array(xScroll,yScroll)
}

function resizeOverlay(){
  $('#fb_box_overlay').css('height',$('body').height());
  $('#fb_box_overlay').css('width',$('body').width());
}

function hideFbModal(){
  $('#fb_box_overlay').hide();
  $('#pd_content_outer').hide();
}

//init
$(document).ready(function(){
  var hash = window.location.hash.toString();
  if(hash == '#pd'){
    window.location = 'plans-pricing.html#pd'
    showFbModal();
  }
});