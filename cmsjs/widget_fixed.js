var _MC_ccapp_toggle = function() {
  var tab = document.getElementById('_MC_ccapp_tab');
  var ccapp = document.getElementById('_MC_ccapp');
  if (ccapp.style.right == '-484px') {
    ccapp.style.right = '0px';
    tab.style.right = '483px';
  } else {
    ccapp.style.right = '-484px';
    tab.style.right = '0px';
  }
};
var _MC_widgetize_ccapp = function() {
  var host = 'http://ccapp.masteryconnect.com?domain='+window.location.host+'&path='+window.location.pathname;
  var container = document.getElementById('_MC_ccapp_widget');
  var showWidget = function() {
    container.innerHTML = '<div id="_MC_ccapp_tab" onclick="_MC_ccapp_toggle()" style="display:block; background:#E5E5E5 url(http://www.masteryconnect.com/images/ccapp_tab.png) no-repeat center center; position:absolute; padding: 6px 0px; top:325px; right:0px; z-index:9999; cursor:pointer; height:134px; width:30px; border-top-left-radius:7px; border-bottom-left-radius:7px; border:1px solid #999; text-shadow: 1px 0px 1px #fff; -webkit-transition: all 0.6s ease-out; -moz-transition: all 0.6s ease-out;"></div><iframe id="_MC_ccapp" style="position:fixed; top:0; right:-484px; z-index:9999; width:480px; height: 100%; border:0; border-left:4px solid #999; -webkit-transition: all 0.6s ease-out; -moz-transition: all 0.6s ease-out;" src="http://ccapp.masteryconnect.com"></iframe>';
  };
  var showItunesApp = function() {
    container.innerHTML = '<a href="http://itunes.apple.com/us/app/common-core-standards/id439424555?mt=8" id="_MC_ccapp_tab" style="display:block; background:#E5E5E5 url(http://www.masteryconnect.com/images/ccapp_tab.png) no-repeat center center; position:fixed; padding: 6px 0px; top:45%; right:-1px; z-index:9999; cursor:pointer; height:134px; width:30px; border-top-left-radius:7px; border-bottom-left-radius:7px; border:1px solid #999; text-shadow: 1px 0px 1px #fff; -webkit-transition: all 0.6s ease-out; -moz-transition: all 0.6s ease-out;"></a>';
  };
  var showAndroidApp = function() {
    container.innerHTML = '<a target="_blank" href="https://market.android.com/details?id=com.masteryconnect.CommonCore" id="_MC_ccapp_tab" style="display:block; background:#E5E5E5 url(http://www.masteryconnect.com/images/ccapp_tab.png) no-repeat center center; position:fixed; padding: 6px 0px; top:45%; right:-1px; z-index:9999; cursor:pointer; height:134px; width:30px; border-top-left-radius:7px; border-bottom-left-radius:7px; border:1px solid #999; text-shadow: 1px 0px 1px #fff; -webkit-transition: all 0.6s ease-out; -moz-transition: all 0.6s ease-out;"></a>';
  };

  var platforms = {
    'iOS': { reg: /(iPad|iPhone)/i, action: showItunesApp },
    'OSX': { reg: /Mac/i, action: showWidget },
    'android': { reg: /Linux arm/i, action: showAndroidApp },
    'windows': { reg: /win/i, action: showWidget },
    'linux': { reg: /(i386|i686|x86_64)/i, action: showWidget }
  };
  for (var i in platforms) {
    if (platforms[i]['reg'] && platforms[i].reg.test(navigator.platform)) {
      platforms[i].action();
      break;
    }
  }
};

if (window.onload) {
  var tmp = window.onload;
  window.onload = function() {
    tmp();
    _MC_widgetize_ccapp();
  }
} else {
  window.onload = function() {
    _MC_widgetize_ccapp();
  }
}
