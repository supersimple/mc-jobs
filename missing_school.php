<?php

$to = "all@masteryconnect.com";
$subject = "Missing School";
$body = "Missing a school.\n\n";
$body .= "\r\nNAME     : ".$_POST['data']['name'];
$body .= "\r\nEMAIL    : ".$_POST['data']['email'];
$body .= "\r\n";
$body .= "\r\nSCHOOL   : ".$_POST['data']['school'];
$body .= "\r\nDISTRICT : ".$_POST['data']['district'];
$body .= "\r\nSTATE    : ".$_POST['data']['state'];
$body .= "\r\nADDRESS  : ".$_POST['data']['address'];
$body .= "\r\nZIP CODE : ".$_POST['data']['zip'];
$body .= "\r\nPHONE    : ".$_POST['data']['school_phone'][0].'-'.$_POST['data']['school_phone'][1].'-'.$_POST['data']['school_phone'][2];
$headers = 'From: noreply@masteryconnect.com'."\r\n".
  'Reply-To: noreply@masteryconnect.com'."\r\n".
  'X-Mailer: PHP/' . phpversion();

if (mail($to, $subject, $body, $headers)) {
  header("Location: ".$_POST['redirect']);
}

?>
